extern crate aimpremote;
extern crate chrono;
extern crate ini;
extern crate hyper;
extern crate url;

use std::io::Read;
use std::time::{Duration};
use std::thread;

use aimpremote::{State, TrackInfo};
use chrono::{Local};
use ini::Ini;
use hyper::Client;
use hyper::header::{ContentType, Headers};
use url::form_urlencoded;

fn send_track_info(login: String, token: String, track: TrackInfo) -> String {
    let url = "http://annimon.com/json/nowplay.php";
    let client = Client::new();
    let query = vec![
        ("act", "set".to_string()),
        ("login", login),
        ("token", token),
        ("artist", track.artist),
        ("title", track.title),
        ("genre", track.genre)
    ];
    let body = form_urlencoded::Serializer::new(String::new())
            .extend_pairs(query)
            .finish();
    let mut headers = Headers::new();
    headers.set(ContentType::form_url_encoded());
    let mut response = client.post(url)
            .headers(headers)
            .body(&body)
            .send()
            .unwrap();
    let mut response_body = String::new();
    response.read_to_string(&mut response_body).unwrap();
    response_body
}

fn read_config() -> Result<(String, String, u32), String> {
    let config = match Ini::load_from_file("config.properties") {
        Ok(config) => config,
        Err(_) => {
            return Err("Unable to open config.properties file".to_owned());
        }
    };
    let section = config.general_section();

    let login = section.get("login");
    let token = section.get("token");
    let time = section.get("update_time").map(|t| t.parse::<u32>());
    match (login, token, time) {
        (Some(l), Some(t), Some(Ok(a))) => Ok((l.to_string(), t.to_string(), a)),
        (Some(login), Some(token), _) => Ok((login.to_string(), token.to_string(), 2)),
        (None, _, _) => Err("Unable to find login".to_owned()),
        (_, None, _) => Err("Unable to find token".to_owned())
    }
}

fn main() {
    let (login, token, time) = match read_config() {
        Ok((login, token, time)) => (login, token, time),
        Err(e) => {
            println!("Error: {}", e);
            return
        }
    };
    println!("Starting aNMusic");

    let update_duration = Duration::from_secs(60 * time as u64);
    let update_duration_if_paused = Duration::from_secs(10);
    loop {
        while {
            let state = aimpremote::get_state().unwrap_or(State::Stopped);
            (state == State::Stopped) || (state == State::Paused)
        } {
            thread::sleep(update_duration_if_paused);
        }
        if let Some(track) = aimpremote::aimp_track_info() {
            println!("[{}] {} - {}", Local::now().format("%H:%M").to_string(), track.artist, track.title);
            let result = send_track_info(login.clone(), token.clone(), track);
            if result.starts_with("{\"error") {
                println!("{}", result);
                break;
            };
        }
        thread::sleep(update_duration);
    }
}
